<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Str;

class HomeController extends Controller
{

    public function toMain()
    {
        return redirect('/');
    }

    public function main()
    {
        return view('welcome',['products'=>\App\Product::inRandomOrder()->first(),
                'client'=>$this->rand_name().' '.$this->rand_name().' '.$this->rand_name()]);
    }

    public function rand_name()
    {
        return ucfirst(mb_strtolower(preg_replace("/[^a-zA-Z]+/", "", Str::random(mt_rand(10,13)))));
    }
}
