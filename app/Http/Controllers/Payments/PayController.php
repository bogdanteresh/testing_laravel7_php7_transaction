<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\PaymentStatuses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PayController extends Controller
{
    private $validation_client_product_arr = [
        'client' => 'required|max:255',
        'product' => 'required|max:255',
        'price_cents' => 'required|numeric|gt:0',//replace comma to dot
        'product_id'=> 'required|integer|exists:products,id'
    ];
    private $validation_bank_card_arr = [
        'card_number' => ['required','regex:/^\d{16}$/'],
        'mm_yyyy' => ['required','regex:/^(0[1-9]|1[0-2])\/(202[3-9]|203[0-9]|204[0-9]|2050)$/'],
        'cvv' => ['required','regex:/^\d{3,4}$/'],
    ];

    public function pay(Request $request)
    {
        if($request->price_cents){
            $request->merge(['price_cents' => str_replace(',','.',$request->price_cents)]);
        }

        $validator = Validator::make($request->all(), $this->validation_client_product_arr);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        return view('bank_card_credentials', ['client'=>$request->client,'price_cents'=>$request->price_cents,
            'product'=>$request->product,'product_id'=>$request->product_id]);

    }

    public function checkout(Request $request)
    {

        $validator = Validator::make($request->all(),
            array_merge($this->validation_client_product_arr,$this->validation_bank_card_arr));

        if ($validator->fails()) {
            return view('bank_card_credentials', ['client'=>$request->client,'price_cents'=>$request->price_cents,
                'product'=>$request->product,'product_id'=>$request->product_id])->withErrors($validator);
        }

        $bank_response = PaymentStatuses::inRandomOrder()->first();
        $qty = 1;
        $sum = str_replace('.','',$request->price_cents) * $qty;
        $uniq = uniqid().mt_rand(100000,999999);

        DB::beginTransaction();
        /**
         * simplification - only 1 product in each invoice
         */
        try {
            $id = DB::table('payments')->insertGetId([
                'sum_total_cents' => $sum,
                'bank_id' => 1,#simp
                'payment_status_id' => $bank_response->id,
            ]);

            foreach ([1/*array of products*/] as $product){
                DB::table('invoices')->insert([
                    'uiid'=>$uniq,
                    'product_id'=>$request->product_id,
                    'quantity'=>$qty,#simp
                    'sum_cents'=>$sum,#simp
                    'client'=>$request->client,
                    'payment_id'=>$id
                ]);
            }

            DB::commit();
            // all good
        } catch (\Throwable $e /* for php 8: \Exception $e*/) {
            DB::rollback();
            // something went wrong
        }

        return view('pay_result',['result'=>__('payments.'.$bank_response->title)]);

    }


}
