<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public $timestamps = true;
    protected $fillable = ['uiid','product_id','quantity','sum_cents','client','payment_id'];
    protected  $hidden = ['created_at','updated_at'];

}
