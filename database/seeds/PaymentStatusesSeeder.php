<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'payment_statuses';
        $statuses = ['success','no_money','bank_refused_payment'];

        foreach ($statuses as $status){
            if(DB::table($table)->where('title',$status)->first() == null){
                DB::table($table)->insert([ 'title' => $status ]);
            }
        }
    }
}
